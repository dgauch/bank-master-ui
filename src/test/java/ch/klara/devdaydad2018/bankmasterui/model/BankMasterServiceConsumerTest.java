package ch.klara.devdaydad2018.bankmasterui.model;

import au.com.dius.pact.consumer.ConsumerPactTestMk2;
import au.com.dius.pact.consumer.MockServer;
import au.com.dius.pact.consumer.dsl.PactDslWithProvider;
import au.com.dius.pact.model.RequestResponsePact;
import static groovy.util.GroovyTestCase.assertEquals;
import static io.pactfoundation.consumer.dsl.LambdaDsl.newJsonArray;
import java.io.IOException;
import java.io.StringReader;
import java.util.HashMap;
import java.util.Map;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.Response;

public class BankMasterServiceConsumerTest extends ConsumerPactTestMk2 {

    @Override
    protected RequestResponsePact createPact(PactDslWithProvider builder) {
        Map<String, String> headers = new HashMap<>();
        headers.put("Content-Type", "application/json");
        
        return builder.given("Bank master initialized")
                .uponReceiving("a request with query param")
                .path("/banks")
                .query("bankClearingNumber=100")
                .method("GET")
                .willRespondWith()
                .status(200)
                .headers(headers)
                .body(newJsonArray((rootArray) -> {
                    rootArray.object((o) -> {
                        o.numberValue("bankClearingNumber", 100);
                        o.stringValue("name", "Schweizerische Nationalbank");
                    });
                }).build())
                .toPact();
    }

    @Override
    protected String providerName() {
        return "bank-master-service";
    }

    @Override
    protected String consumerName() {
        return "bank-master-ui";
    }

    @Override
    protected void runTest(MockServer mockServer) throws IOException {
        Response response = ClientBuilder.newBuilder().build()
                .target(mockServer.getUrl()).path("banks")
                .queryParam("bankClearingNumber", "100")
                .request().get();

        assertEquals("HTTP response code", 200, response.getStatus());

        String jsonString = response.readEntity(String.class);
        JsonArray responseJsonArray = Json.createReader(new StringReader(jsonString)).readArray();

        assertEquals("Expecting 1 result", 1, responseJsonArray.size());

        JsonObject bankJson = responseJsonArray.getJsonObject(0);
        assertEquals("Bank clearing number", 100, bankJson.getInt("bankClearingNumber"));
        assertEquals("Name of the bank", "Schweizerische Nationalbank", bankJson.getString("name"));
    }

}
