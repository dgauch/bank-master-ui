package ch.klara.devdaydad2018.bankmasterui.control;

import java.util.List;
import java.util.stream.Collectors;
import javax.enterprise.inject.Model;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;

@Model
public class IndexViewBean {

    String enteredString;
    String echoedString;

    String enteredBankClearingNumber;
    List<String> banksList;

    public String consumeEchoService() {
        if (enteredString == null || enteredString.length() == 0) {
            setEchoedString(null);
        } else {
            echoedString
                    = ClientBuilder.newBuilder().build()
                            .target("http://localhost:8081")
                            .path("echo")
                            .path(enteredString)
                            .request()
                            .get(String.class);
        }
        return null;
    }

    public String consumeBanksService() {
        WebTarget target = ClientBuilder.newBuilder().build()
                .target("http://localhost:8081")
                .path("banks");
        if (enteredBankClearingNumber != null && enteredBankClearingNumber.length() > 0) {
            target = target.queryParam("bankClearingNumber", enteredBankClearingNumber);
        }
        JsonArray banksJsonArray = target.request().get(JsonArray.class);

        this.banksList = bankJsonArrayToStrings(banksJsonArray);

        return null;
    }

    private List<String> bankJsonArrayToStrings(JsonArray banksJsonArray) {
        return banksJsonArray.getValuesAs(JsonObject.class).stream()
                .map(b -> String.format("%s (%d)", b.getString("name"), b.getInt("bankClearingNumber")))
                .collect(Collectors.toList());
    }

    public List<String> getBanksList() {
        return this.banksList;
    }

    public String getEnteredString() {
        return enteredString;
    }

    public void setEnteredString(String enteredString) {
        this.enteredString = enteredString;
    }

    public String getEchoedString() {
        return echoedString;
    }

    public void setEchoedString(String echoedString) {
        this.echoedString = echoedString;
    }

    public String getEnteredBankClearingNumber() {
        return enteredBankClearingNumber;
    }

    public void setEnteredBankClearingNumber(String enteredBankClearingNumber) {
        this.enteredBankClearingNumber = enteredBankClearingNumber;
    }

}
