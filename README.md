# How to build

```
 mvn clean install
```

# How to run 

```
java -jar target/bank-master-ui-swarm.jar
```

Pacts are available in the following directory:
```
target/pacts 
```

Open in your browser:
- http://localhost:8084
